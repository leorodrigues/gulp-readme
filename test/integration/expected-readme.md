

# Foreword

Sample file for integration testing.

Lets have a code snippet!

```javascript

// some neat comment here

console.log('hello world');

```

And here is a link to [google](https://www.google.com)!

# Api Reference



## dev/guinea-pig
---

My guinea-pig jsdoc module used in the construction of the plugins

## Summary

 - [makeSomeOtherFunction](#module:dev/guinea-pig.makeSomeOtherFunction)
 - [theOtherFunction](#module:dev/guinea-pig.makeSomeOtherFunction~theOtherFunction)
 - [Person](#module:dev/guinea-pig.Person)
   - [constructor](#module:dev/guinea-pig.Person#constructor)
   - [MAX_AGE](#module:dev/guinea-pig.Person#MAX_AGE)
   - [sayLine](#module:dev/guinea-pig.Person#sayLine)
 - [Employee](#module:dev/guinea-pig.Employee)
   - [constructor](#module:dev/guinea-pig.Employee#constructor)

### (_static_) makeSomeOtherFunction
---

A factory function

**Parameters**

- **bound** - _any_ - Sample bound parameter



### (_inner_) theOtherFunction
---

A product function

**Parameters**

- **unbound** - _string_ - Sample free parameter

**Returns**

 - **Array** - A tuple of (bound, unbound).


## (_static_) Person

A very common class

### (_static_) constructor
---

Makes a new person

**Parameters**

- **name** - _string_ - The person&#39;s name
- **age** - _number_ - The person&#39;s age



### (_instance_) MAX_AGE
---

A person's maximum age




### (_instance_) sayLine
---

Sends a message line through a stream

**Parameters**

- **message** - _String_ - 
**Returns**

 - **String**
**Throws**

 - **Error** - Randomly
 - **SomeOtherError** - Hourly


## (_static_) Employee

A derived class

### (_static_) constructor
---

Makes a new employee

**Parameters**

- **name** - _string_ - The employee&#39;s name
- **age** - _number_ - The emplyee&#39;s age
- **salary** - _number_ - The employee&#39;s salary



