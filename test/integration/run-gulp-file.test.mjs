import gulp from 'gulp';
import { readFileSync } from 'fs';
import { join } from 'path';
import { configureGulp } from './gulp-lib.mjs';
import { expect } from 'chai';

const BASE_PATH = import.meta.dirname;
const filename = join(BASE_PATH, 'expected-readme.md');
const expectedReadme = readFileSync(filename, 'utf-8');

describe('integration/gulpfile', function () {

    configureGulp(gulp);

    afterEach(() => gulp.task('clean')());

    it('Should generate a full readme', async () => {

        const productReadme = await new Promise((resolve, reject) => {
            const docsTask = gulp.task('test-docs');
            const task = gulp.series(docsTask, taskDone => {
                try {
                    const inputFilePath = join(BASE_PATH, 'build', 'README.md');
                    const productReadme = readFileSync(inputFilePath, 'utf-8');
                    taskDone();
                    resolve(productReadme);
                } catch(error) {
                    taskDone();
                    reject(error);
                }
            });
            task();
        });

        expect(productReadme).to.be.equal(expectedReadme);
    });
});