import gulpClean from 'gulp-clean';
import path from 'path';
import { makeGulpReadme, declareTasks } from '../../index.mjs';

const PROJECT_NAME = 'test';

const BASE_PATH = import.meta.dirname;

const BUILD_PATH = path.join(BASE_PATH, 'build');

const JS_SOURCES = [
    path.join(BASE_PATH, 'guinea-pig.mjs')
];

const NOTES_SOURCES = [
    path.join(BASE_PATH, 'notes', 'foreword.md')
];

const REF_SOURCES = [
    path.join(BASE_PATH, 'notes', 'references.json')
];

const ALL_SOURCES = {
    jsSources: JS_SOURCES,
    notesSources: NOTES_SOURCES,
    refSources: REF_SOURCES
};

export function configureGulp(gulp) {
    const gReadme = makeGulpReadme();

    const defaultDocsTask = declareTasks(
        PROJECT_NAME, gulp, gReadme, BUILD_PATH, ALL_SOURCES);

    gulp.task(`${PROJECT_NAME}-docs`, gulp.series([defaultDocsTask], function() {
        return gulp.src(`${BUILD_PATH}/readme.md`);
    }));

    gulp.task('clean', function() {
        return gulp.src(`${BUILD_PATH}/`, {read: false})
            .pipe(gulpClean({force: true}));
    });

    gulp.task('default', gulp.series([`${PROJECT_NAME}-docs`]));
}

