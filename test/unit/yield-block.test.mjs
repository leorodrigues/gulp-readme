import { expect } from 'chai';
import fs from 'fs';
import path from 'path';
import { yieldBlock } from '../../lib/text-utils.mjs';

const BASE_DIR = import.meta.dirname;

function loadSample(fileName) {
    const inputFilePath = path.join(BASE_DIR, 'yield-block-samples', fileName);
    return fs.readFileSync(inputFilePath, 'utf-8');
}

describe('text-utils/yieldBlock', function () {
    describe('Given two titles', function () {
        it('Should yield two title blocks', () => {
            const inputText = loadSample('title-two-instances.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                '# First section',
                '# Second section'
            ]);
        });
    });

    describe('Given one title', function () {
        it('Should yield one title block', () => {
            const inputText = loadSample('title-one-instance.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                '# This is the title'
            ]);
        });
    });

    describe('Given two code snippets as input', function () {
        it('Should yield two code snippet blocks', () => {
            const inputText = loadSample('code-snippet-two-instances.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                '```bash\n\necho hello world\n\n```',
                '```javascript\n\n// nothing to see here\nconsole.log(\'hello world\')\n\n```'
            ]);
        });
    });

    describe('Given one code snippet as input', function () {
        it('Should yield one code snippet block', () => {
            const inputText = loadSample('code-snippet-one-instance.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                '```bash\n\necho hello world\n\n```'
            ]);
        });
    });

    describe('Given two numbered lists as input', function () {
        it('Should yield two numbered list blocks', () => {
            const inputText = loadSample('number-list-two-instances.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                ' 1. Jack\n 2. John\n 3. Philip',
                ' 4. Barbra\n 5. Karen\n 6. Paula',
            ]);
        });
    });

    describe('Given a single numbered input list', function () {
        it('Should yield one numbered list block', () => {
            const inputText = loadSample('number-list-one-instance.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                ' 1. Jack\n 2. John\n 3. Philip'
            ]);
        });
    });

    describe('Given a doulbe bullet input', function () {
        it('Should yield two bullet blocks', () => {
            const inputText = loadSample('bullet-list-two-instances.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                ' - First\n - Second\n - Third',
                ' - Forth\n - Fifth\n - Sixth'
            ]);
        });
    });

    describe('Given a single bullet', function () {
        it('Should yield a single bullet block', () => {
            const inputText = loadSample('bullet-list-one-instance.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                ' - First\n - Second\n - Third'
            ]);
        });
    });

    describe('Given two text blocks', function () {
        it('Should yield two lines', () => {
            const inputText = loadSample('paragraph-two-instances.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                'This is the first line',
                'This is the second line'
            ]);
        });
    });

    describe('Given a text block', function () {
        it('Should yield a single line', () => {
            const inputText = loadSample('paragraph-one-instance.md');

            const output = [...yieldBlock(inputText.split('\n'))];

            expect(output).to.be.deep.equal([
                'This is a paragraph made of three lines'
            ]);
        });
    });
});