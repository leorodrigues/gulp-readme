import mapStream from 'map-stream';
import path from 'path';
import { expect } from 'chai';

import { loadJson } from '@leorodrigues/test-support';
import { Readable } from 'stream';
import { makeReduceExplanationApiCall } from '../../lib/reduce-explanation.mjs';
import { makeExplainSourceApiCall } from '../../lib/explain-source.mjs';
import { makeGenerateReadmeModelApiCall } from '../../lib/generate-readme-model.mjs';

import vinylFs from 'vinyl-fs';

const guineaPigCommonJSReducedJson =
    loadJson(path.join(import.meta.dirname, 'guinea-pig.commonjs.reduced.json'));

const guineaPigES6ModuleReducedJson =
    loadJson(path.join(import.meta.dirname, 'guinea-pig.es6module.reduced.json'));

const generateReadmeModel = makeGenerateReadmeModelApiCall();
const reduceExplanation = makeReduceExplanationApiCall();
const explainSource = makeExplainSourceApiCall();

const dirname = import.meta.dirname;
const filename = import.meta.filename.replace('test.mjs', 'data.json');
const explanation = loadJson(filename);

describe('lib/reduce-explanation', () => {
    it('Should work for a CommonJS file', (done) => {
        vinylFs.src(path.join(dirname, 'guinea-pig.js'))
            .pipe(explainSource())
            .pipe(reduceExplanation())
            .pipe(mapStream(file => {
                expect(file.explanation).to.be.deep.equal(guineaPigCommonJSReducedJson);
                done();
            }));
    });

    it('Should work for a ES6 module file', (done) => {
        vinylFs.src(path.join(dirname, 'guinea-pig.mjs'))
            .pipe(explainSource())
            .pipe(reduceExplanation())
            .pipe(mapStream(file => {
                expect(file.explanation).to.be.deep.equal(guineaPigES6ModuleReducedJson);
                done();
            }));
    });

    it('Should work also', () => {
        const stream = new Readable({
            objectMode: true,
            read() {
                this.push({ api: [guineaPigCommonJSReducedJson] });
                this.push(null);
            }
        });

        return stream
            .pipe(generateReadmeModel())
            .pipe(mapStream(readme => {
                const actual = JSON.stringify(readme);
                const expected = JSON.stringify(explanation);
                expect(actual).to.be.deep.equal(expected);
            }));
    });
});