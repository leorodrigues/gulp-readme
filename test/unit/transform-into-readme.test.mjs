import path from 'path';
import { loadJson } from '@leorodrigues/test-support';
import { expect } from 'chai';

import { transformIntoReadme }
    from '../../lib/generate-readme-model.mjs';

const loadLocalJson = fileName =>
    loadJson(path.join(import.meta.dirname, fileName));

const moduleWithFunctionOnly = loadLocalJson('module-with-function-only.json');

const moduleWithClassOnly = loadLocalJson('module-with-class-only.json');

const moduleWithConstantOnly = loadLocalJson('module-with-constant-only.json');

describe('docs/src/lib/transform-into-readme', () => {
    describe('#transformIntoReadme', () => {
        it('Should process one api record with only an instance', () => {
            const readme = transformIntoReadme({
                api: [moduleWithConstantOnly]
            });
            const sections = [{
                title: 'Api Reference',
                target: 'reference',
                content: [{
                    type: 'summary',
                    content: [{
                        type: 'link',
                        text: 'dev/guinea-pig',
                        ref: 'module:dev/guinea-pig'
                    }]
                },{
                    type: 'module',
                    name: 'dev/guinea-pig',
                    target: 'module:dev/guinea-pig',
                    summary: [{
                        type: 'link',
                        text: 'SOME_STRING',
                        ref: 'module:dev/guinea-pig~SOME_STRING'
                    }],
                    description: [{
                        type: 'para',
                        content: [{
                            type: 'text',
                            value: 'My guinea-pig jsdoc module'
                        }]
                    }],
                    content: [{
                        type: 'instance',
                        name: 'SOME_STRING',
                        scope: 'inner',
                        target: 'module:dev/guinea-pig~SOME_STRING',
                        description: [{
                            type: 'para',
                            content: [{
                                type: 'text',
                                value: 'A random string'
                            }]
                        }]
                    }]
                }]
            }];
            expect(readme).to.be.deep.equals({sections});
        });

        it('Should process one api record with only a class', () => {
            const readme = transformIntoReadme({
                api: [moduleWithClassOnly]
            });
            const sections = [{
                title: 'Api Reference',
                target: 'reference',
                content: [{
                    type: 'summary',
                    content: [{
                        type: 'link',
                        text: 'lib/another-module',
                        ref: 'module:lib/another-module'
                    }]
                },{
                    type: 'module',
                    name: 'lib/another-module',
                    target: 'module:lib/another-module',
                    summary: [{
                        type: 'link',
                        text: 'MyClass',
                        ref: 'module:lib/another-module~MyClass',
                        menu: [{
                            'ref': 'module:lib/another-module~MyClass#constructor',
                            'text': 'constructor',
                            'type': 'link',
                        },{
                            'ref': 'module:lib/another-module~MyClass#say',
                            'text': 'say',
                            'type': 'link',
                        }]
                    }],
                    description: [{
                        type: 'para',
                        content: [{ type: 'text', value: 'Exports a single class' }]
                    }],
                    content: [{
                        type: 'class',
                        name: 'MyClass',
                        target: 'module:lib/another-module~MyClass',
                        scope: 'inner',
                        description: [{
                            type: 'para',
                            content: [{ type: 'text', value: 'A class invented by me.' }]
                        }],
                        ancestors: ['AnotherClass'],
                        content: [{
                            type: 'constructor',
                            name: 'constructor',
                            target: 'module:lib/another-module~MyClass#constructor',
                            scope: 'inner',
                            description: [{
                                type: 'para',
                                content: [{
                                    type: 'text',
                                    value: 'Creates a new instance of MyClass'
                                }]
                            }],
                            params: [{
                                type: 'string',
                                name: 'message',
                                description: [{
                                    type: 'para',
                                    content: [{
                                        type: 'text',
                                        value: 'Something to print on the screen.'
                                    }]
                                }]
                            }]
                        },{
                            type: 'method',
                            name: 'say',
                            target: 'module:lib/another-module~MyClass#say',
                            scope: 'instance',
                            description: [{
                                type: 'para',
                                content: [{ type: 'text', value: 'Computes a message.' }]
                            }],
                            returns: {
                                type: 'String',
                                description: [{
                                    type: 'para',
                                    content: [{ type: 'text', value: 'The message to be printed.' }]
                                }]
                            }
                        }]
                    }]
                }]
            }];
            expect(readme).to.be.deep.equals({sections});
        });

        it('Should process one api record with only a function', () => {
            const readme = transformIntoReadme({
                api: [moduleWithFunctionOnly]
            });
            const sections = [{
                title: 'Api Reference',
                target: 'reference',
                content: [{
                    type: 'summary',
                    content: [{
                        type: 'link',
                        text: 'lib/some-module',
                        ref: 'module:lib/some-module'
                    }]
                },{
                    type: 'module',
                    name: 'lib/some-module',
                    target: 'module:lib/some-module',
                    summary: [{
                        type: 'link',
                        text: 'doSomething',
                        ref: 'module:lib/some-module~doSomething'
                    }],
                    description: [{
                        type: 'para',
                        content: [{ type: 'text', value: 'Some text paragraph' }]
                    }],
                    content: [{
                        type: 'function',
                        name: 'doSomething',
                        target: 'module:lib/some-module~doSomething',
                        scope: 'inner',
                        description: [{
                            type: 'para',
                            content: [{ type: 'text', value: 'The name says it all.' }]
                        },{
                            type: 'para',
                            content: [{ type: 'text', value: 'A very important piece of code.' }]
                        }],
                        params: [{
                            type: 'Object',
                            name: 'paramOne',
                            description: [{
                                type: 'para',
                                content: [{ type: 'text', value: 'It is the first.' }]
                            }]
                        },{
                            type: 'Array',
                            name: 'paramTwo',
                            description: [{
                                type: 'para',
                                content: [{ type: 'text', value: 'You guess' }]
                            }]
                        }],
                        returns: {
                            type: 'Array',
                            description: [{
                                type: 'para',
                                content: [{ type: 'text', value: 'The result' }]
                            }]
                        }
                    }]
                }]
            }];
            expect(readme).to.be.deep.equals({sections});
        });

        it('Should process one note with one code snippet', () => {
            const readme = transformIntoReadme({
                notes: [
                    [{
                        type: 'code',
                        content: [{
                            type: 'text',
                            value: '```bash\necho hello\n```'
                        }]
                    }]
                ]
            });
            const sections = [{
                title: 'Foreword',
                target: 'foreword',
                content: [{
                    type: 'code',
                    content: [{
                        type: 'text',
                        value: '```bash\necho hello\n```'
                    }]
                }]
            }];
            expect(readme).to.be.deep.equals({sections});
        });

        it('Should process one note with one paragraph', () => {
            const readme = transformIntoReadme({
                notes: [
                    [{
                        type: 'para',
                        content: [{
                            type: 'text',
                            value: 'some text i wrote'
                        }]
                    }]
                ]
            });
            const sections = [{
                title: 'Foreword',
                target: 'foreword',
                content: [{
                    type: 'para',
                    content: [{
                        type: 'text',
                        value: 'some text i wrote'
                    }]
                }]
            }];
            expect(readme).to.be.deep.equals({sections});
        });

        it('Should process one note with two paragraphs', () => {
            const readme = transformIntoReadme({
                notes: [
                    [{
                        type: 'para',
                        content: [{
                            type: 'text',
                            value: 'first paragraph'
                        }]
                    },{
                        type: 'para',
                        content: [{
                            type: 'text',
                            value: 'second paragraph'
                        }]
                    }]
                ]
            });
            const sections = [{
                title: 'Foreword',
                target: 'foreword',
                content: [{
                    type: 'para',
                    content: [{
                        type: 'text',
                        value: 'first paragraph'
                    }]
                },{
                    type: 'para',
                    content: [{
                        type: 'text',
                        value: 'second paragraph'
                    }]
                }]
            }];
            expect(readme).to.be.deep.equals({sections});
        });

        it('Should process two notes with one paragraph', () => {
            const readme = transformIntoReadme({
                notes: [
                    [{
                        type: 'para',
                        content: [{
                            type: 'text',
                            value: 'first note'
                        }]
                    }],[{
                        type: 'para',
                        content: [{
                            type: 'text',
                            value: 'second note'
                        }]
                    }]
                ]
            });
            const sections = [{
                title: 'Foreword',
                target: 'foreword',
                content: [{
                    type: 'para',
                    content: [{
                        type: 'text',
                        value: 'first note'
                    }]
                },{
                    type: 'para',
                    content: [{
                        type: 'text',
                        value: 'second note'
                    }]
                }]
            }];
            expect(readme).to.be.deep.equals({sections});
        });

        it('Should process empty input', () => {
            const readme = transformIntoReadme({});
            expect(readme).to.be.deep.equals({sections: []});
        });
    });
});
