/**
 * My guinea-pig jsdoc module used in the construction of the plugins
 * @module dev/guinea-pig
 * @author Teixeira, Leonardo Rodrigues <leorodriguesrj@gmail.com>
 */

/** A random string */
export const SOME_STRING = 'this is my string';

/** A fibonacci sequence in integer form */
export const SOME_NUMBER = 112358;

/** My favorite fibonacci sequence in Array form */
export const FIBS = [1, 1, 2, 3, 5, 8];

/** A person record */
export const SIMPLE_OBJECT = { age: 32, name: 'jhon' };

/**
 * A very common class
 */
export class Person {

    /** A person's maximum age */
    get MAX_AGE() {
        return 120;
    }

    /**
     * Makes a new person
     * @param {string} name The person's name
     * @param {number} age The person's age
     */
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    /**
     * Sends a message line through a stream
     * @param {String} message
     * @return {String}
     * @throws {Error} Randomly
     * @throws {SomeOtherError} Hourly
     */
    sayLine(message) {
        return `${this.name} says: ${message}\n`;
    }
}

/**
 * A derived class
 * @extends Person
 */
export class Employee extends Person {
    /**
     * Makes a new employee
     * @param {string} name The employee's name
     * @param {number} age The emplyee's age
     * @param {number} salary The employee's salary
     */
    constructor(name, age, salary) {
        super(name, age);
        this.salary = salary;
    }
}

/**
 * A factory function
 * @param {any} bound Sample bound parameter
 */
export function makeSomeOtherFunction(bound) {
    /**
     * A product function
     * @param {string} unbound Sample free parameter
     * @returns {Array} A tuple of (bound, unbound).
     */
    function theOtherFunction(unbound) {
        return [bound, unbound];
    }

    return theOtherFunction;
}

const JACK = new Person('Jack', 21);
