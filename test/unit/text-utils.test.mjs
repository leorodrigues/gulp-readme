import { expect, use } from 'chai';
import sinon from 'sinon';

import {
    streamLineElements,
    streamOutBulletList,
    streamOutNumberList,
    streamOutParagraph,
    streamOutTextChunks,
    streamOutCodeSnippet,
    streamOutTitle,
    thereIsBulletedItemAt,
    thereIsNumberedItemAt,
    makeBulletedItem,
    makeNumberedItem,
    makeDefaultRecognizer
} from '../../lib/text-utils.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();
const fakeStreamOut = sandbox.stub();

const groceries = [
    ' - A dozen eggs',
    ' + 1kg of beef',
    ' * 6 tomatoes'
].join('\n');

const topThreeAiCharacters = [
    '1. HAL9000 - In the movie "2001: A Space Odyssey",',
    ' this disenbodied AI is my favorite.',
    '2. GlaDOS - From the "Portal" games, was once a human.',
    '3. Cabal - The sinister Brotherhood of Nod\'s advisor.'
].join('\n');

describe('docs/src/lib/text-utils', () => {
    describe('streamOutTextChunks', () => {
        afterEach(() => sandbox.reset());
        it('Break text into chunks', () => {
            fakeStreamOut.returns(['x']);
            const text = 'some\n\nsimple\n\n\ntext';
            const result = [...streamOutTextChunks(text,
                () => () => fakeStreamOut)];
            expect(result).to.be.deep.equal(['x', 'x', 'x']);
        });
    });

    describe('makeDefaultRecognizer', () => {
        const recognize = makeDefaultRecognizer();
        it('Should recognize title', () => {
            expect(recognize('# README')).to.be.equal(streamOutTitle);
            expect(recognize('## FOREWORD')).to.be.equal(streamOutTitle);
            expect(recognize('### API REF')).to.be.equal(streamOutTitle);
        });
        it('Should recognize bulleted list', () => {
            expect(recognize(' - test 1')).to.be.equal(streamOutBulletList);
            expect(recognize(' + test 2')).to.be.equal(streamOutBulletList);
            expect(recognize(' * test 3')).to.be.equal(streamOutBulletList);
        });
        it('Should recognize numbered list', () => {
            expect(recognize('1. Butter'))
                .to.be.equal(streamOutNumberList);
            expect(recognize('20. Refrigerator'))
                .to.be.equal(streamOutNumberList);
        });
        it('Should recognize paragraph', () => {
            expect(recognize('Simple text')).to.be.equal(streamOutParagraph);
        });
        it('Should recognize code snippet', () => {
            expect(recognize('```javascript\nconsole.log(\'hello\');\n```'))
                .to.be.equal(streamOutCodeSnippet);
        });
    });

    describe('streamOutCodeSnippet', () => {
        it('Should yield a numbered list', () => {
            const snippet = `\`\`\`javascript\nconsole.log('hello world');\n\`\`\``;

            const textElement = {
                type: 'text', value: snippet
            };

            expect(...streamOutCodeSnippet(snippet))
                .to.be.deep.equals({ type: 'code', content: [textElement] });
        });
    });

    describe('streamOutNumberList', () => {
        afterEach(() => sandbox.reset());
        it('Should yield a numbered list', () => {
            fakeStreamOut.returns(['x']);
            expect(...streamOutNumberList('some text', fakeStreamOut))
                .to.be.deep.equals({ type: 'numbered', items: ['x'] });
            expect(fakeStreamOut).to.be.called.calledOnceWith(
                'some text', thereIsNumberedItemAt, makeNumberedItem);
        });
    });

    describe('streamOutBulletList', () => {
        afterEach(() => sandbox.reset());
        it('Should yield a bulleted list', () => {
            fakeStreamOut.returns(['y']);
            expect(...streamOutBulletList('some text', fakeStreamOut))
                .to.be.deep.equals({ type: 'bulleted', items: ['y'] });
            expect(fakeStreamOut).to.be.called.calledOnceWith(
                'some text', thereIsBulletedItemAt, makeBulletedItem);
        });
    });

    describe('thereIsBulletedItemAt', () => {
        it('Should recognize an item starting with dash', () => {
            expect(thereIsBulletedItemAt(groceries, 0)).to.be.true;
        });
        it('Should recognize an item starting with plus', () => {
            expect(thereIsBulletedItemAt(groceries, 16)).to.be.true;
        });
        it('Should recognize an item starting with asterix', () => {
            expect(thereIsBulletedItemAt(groceries, 31)).to.be.true;
        });
    });

    describe('thereIsNumberedItemAt', () => {
        it('Should return false at the edge of item continuation.', () => {
            expect(thereIsNumberedItemAt(topThreeAiCharacters, 50)).to.be.false;
        });
        it('Should return true at the edge of new item.', () => {
            expect(thereIsNumberedItemAt(topThreeAiCharacters, 88)).to.be.true;
        });
    });

    describe('makeBulletItem', () => {
        it('Should take a piece from the start of text', () => {
            expect(...makeBulletedItem(groceries, 0, 15)).to.be.deep.equals(
                {content: [{type: 'text', value: 'A dozen eggs'}]});
        });
        it('Should take a piece from the middle', () => {
            expect(...makeBulletedItem(groceries, 16, 30)).to.be.deep.equals(
                {content: [{type: 'text', value: '1kg of beef'}]});
        });
        it('Should take a piece from the end', () => {
            expect(...makeBulletedItem(groceries, 31, 44)).to.be.deep.equals(
                {content: [{type: 'text', value: '6 tomatoes'}]});
        });
    });

    describe('makeNumberedItem', () => {
        it('Should take a piece from the start of text', () => {
            const value = [
                'HAL9000 - In the movie "2001: A Space Odyssey",',
                'this disenbodied AI is my favorite.'
            ].join(' ');
            expect(...makeNumberedItem(topThreeAiCharacters, 0, 87))
                .to.be.deep.equals({ position: '1', content: [{
                    type: 'text', value }] });
        });
        it('Should take a piece from the middle', () => {
            const value = 'GlaDOS - From the "Portal" games, was once a human.';
            expect(...makeNumberedItem(topThreeAiCharacters, 88, 142))
                .to.be.deep.equals({ position: '2', content: [{
                    type: 'text', value }]});
        });
        it('Should take the last item', () => {
            const value = 'Cabal - The sinister Brotherhood of Nod\'s advisor.';
            expect(...makeNumberedItem(topThreeAiCharacters, 143))
                .to.be.deep.equals({ position: '3', content: [{
                    type: 'text', value }]});
        });
    });

    describe('streamLineElements', () => {
        it('Should emit text.', () => {
            expect([...streamLineElements('some text')]).to.be.deep.equals([
                {type: 'text', value: 'some text'}
            ]);
        });
        it('Should emit link.', () => {
            const text = '[somewhere to go]{@link some site address}';
            expect([...streamLineElements(text)]).to.be.deep.equals([{
                type: 'link', text: 'somewhere to go', ref: 'some site address'
            }]);
        });
        it('Should emit text, then link.', () => {
            const text = 'go to [next submenu]{@link #submenu}';
            const result = [...streamLineElements(text)];
            expect(result).to.be.deep.equals([
                {type: 'text', value: 'go to '},
                {type: 'link', text: 'next submenu', ref: '#submenu'}
            ]);
        });
        it('Should emit link, then text.', () => {
            const text = '[click here]{@link http://localhost/x/y} for info';
            const result = [...streamLineElements(text)];
            expect(result).to.be.deep.equals([
                {type: 'link', text: 'click here', ref: 'http://localhost/x/y'},
                {type: 'text', value: ' for info'}
            ]);
        });
        it('Should emit text, link, then text.', () => {
            const text = 'go to [next submenu]{@link #submenu} to see more';
            const result = [...streamLineElements(text)];
            expect(result).to.be.deep.equals([
                {type: 'text', value: 'go to '},
                {type: 'link', text: 'next submenu', ref: '#submenu'},
                {type: 'text', value: ' to see more'}
            ]);
        });
    });
});
