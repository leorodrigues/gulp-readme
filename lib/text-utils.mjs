
/* eslint-disable */
const LINK_PATTERN = /(\[([\w:.#~/'-][\s\w:.#~/',-]*)\]\{@link\s+([\w:.#~/'-][\s\w:.#~/',-]*)\})/gm;
/* eslint-enable */

const NUMBERED_ITEM_PATTERN = /^(\d+)\.\s+(.*)$/;

const notEmptyStrings = e => e !== '';

export function *streamLineElements(text, linkPattern = LINK_PATTERN) {
    const parts = text.split(linkPattern).filter(notEmptyStrings);
    while (parts.length) {
        const value = parts.shift();
        linkPattern.lastIndex = 0;
        if (linkPattern.test(value))
            yield { type: 'link', text: parts.shift(), ref: parts.shift() };
        else
            yield { type: 'text', value };
    }
}

export function *streamOutChunkElements(text, makeRecognizer) {
    const recognize = makeRecognizer(text);
    if (recognize)
        yield *recognize(text);
}

export function *makeNumberedItem(text, begin, end) {
    const slice = text.substring(begin, end)
        .replace(/\n+/gm, ' ')
        .replace(/\s+/g, ' ');
    NUMBERED_ITEM_PATTERN.lastIndex = 0;
    const pieces = slice.match(NUMBERED_ITEM_PATTERN).filter(notEmptyStrings);
    yield { position: pieces[1], content: [...streamLineElements(pieces[2])] };
}

export function *makeBulletedItem(text, begin, end) {
    const slice = text.substring(begin + 3, end).replace(/\n+/gm, ' ');
    yield { content: [...streamLineElements(slice)] };
}

export function thereIsNumberedItemAt(text, start) {
    const numbers = '1234567890';
    for (let i = start; i < text.length; i++) {
        if (numbers.indexOf(text.charAt(i)) > -1)
            continue;
        return text.charAt(i) === '.';
    }
    return false;
}

export function thereIsBulletedItemAt(text, start) {
    return text.charAt(start) === ' '
        && '-+*'.indexOf(text.charAt(start + 1)) > -1
        && text.charAt(start + 2) === ' ';
}

export function *streamOutList(text, identify, makeItems) {
    let start = 0;
    for (let i = start; i < text.length; i++) {
        if (text.charAt(i) === '\n' && identify(text, i + 1)) {
            yield *makeItems(text, start, i);
            start = i + 1;
        }
    }
    yield *makeItems(text, start);
}

export function *streamOutNumberList(text, streamOut = streamOutList) {
    const items = [...streamOut(text, thereIsNumberedItemAt, makeNumberedItem)];
    yield { type: 'numbered', items };
}

export function *streamOutBulletList(text, streamOut = streamOutList) {
    const items = [...streamOut(text, thereIsBulletedItemAt, makeBulletedItem)];
    yield { type: 'bulleted', items };
}

export function *streamOutTitle(text) {
    text = text.replace(/\n+/gm, ' ');
    const content = [...streamLineElements(text.replace(/^[#\s]+/gm, ''))];
    yield { type: 'title', content };
}

export function *streamOutParagraph(text) {
    const content = [...streamLineElements(text.replace(/\n+/gm, ' '))];
    yield { type: 'para', content };
}

export function *streamOutCodeSnippet(text) {
    const content = [...streamLineElements(text)];
    yield { type: 'code', content };
}

export function makeDefaultRecognizer() {
    const numberedListPattern = t => /^\s*\d+\.\s+/g.test(t);
    const bulletedListPattern = t => /^\s+[+*-]\s+/g.test(t);
    const titlePattern = t => /^#+\s+/g.test(t);
    const codeSnippetPattern = t => /^```\w*/g.test(t);
    return text => {
        return titlePattern(text) ? streamOutTitle
            : (bulletedListPattern(text) ? streamOutBulletList
                : (numberedListPattern(text) ? streamOutNumberList
                    : (codeSnippetPattern(text) ? streamOutCodeSnippet
                        : streamOutParagraph)));
    };
}

export function *yieldLines(text) {
    for (const line of text.split('\n'))
        yield line;
}

export function *yieldBlock(lines) {
    const startTitlePattern = l => /^#+\s+/g.test(l);
    const startBulletedListPattern = l => /^\s+[+*-]\s+/g.test(l);
    const startNumberedListPattern = l => /^\s*(\d+|\w+)\.\s+/g.test(l);
    const startCodeSnippetPattern = l => /^```\w*/g.test(l);
    const startParagraph = l => /.+/g.test(l);
    const stopCodeSnippetPattern = l => /^```$/g.test(l);
    const anything = l => /^.*$/g.test(l);
    const emptyLinePattern = l => /^$/g.test(l);

    const DEFAULT = 0;
    const CONSUME_TITLE = 1;
    const CONSUME_BULLET = 2;
    const CONSUME_NUMBER = 3;
    const CONSUME_CODE = 4;
    const CONSUME_PARAGRAPH = 5;

    const transitionTable = [
        [DEFAULT, startTitlePattern, CONSUME_TITLE],
        [CONSUME_TITLE, startBulletedListPattern, CONSUME_BULLET],
        [CONSUME_TITLE, startNumberedListPattern, CONSUME_NUMBER],
        [CONSUME_TITLE, startCodeSnippetPattern, CONSUME_CODE],
        [CONSUME_TITLE, startParagraph, CONSUME_PARAGRAPH],
        [CONSUME_TITLE, emptyLinePattern, DEFAULT],

        [DEFAULT, startBulletedListPattern, CONSUME_BULLET],
        [CONSUME_BULLET, startTitlePattern, CONSUME_TITLE],
        [CONSUME_BULLET, startBulletedListPattern, CONSUME_BULLET],
        [CONSUME_BULLET, startNumberedListPattern, CONSUME_NUMBER],
        [CONSUME_BULLET, startCodeSnippetPattern, CONSUME_CODE],
        [CONSUME_BULLET, startParagraph, CONSUME_BULLET],
        [CONSUME_BULLET, emptyLinePattern, DEFAULT],

        [DEFAULT, startNumberedListPattern, CONSUME_NUMBER],
        [CONSUME_NUMBER, startTitlePattern, CONSUME_TITLE],
        [CONSUME_NUMBER, startNumberedListPattern, CONSUME_NUMBER],
        [CONSUME_NUMBER, startBulletedListPattern, CONSUME_BULLET],
        [CONSUME_NUMBER, startCodeSnippetPattern, CONSUME_CODE],
        [CONSUME_NUMBER, startParagraph, CONSUME_NUMBER],
        [CONSUME_NUMBER, emptyLinePattern, DEFAULT],

        [DEFAULT, startCodeSnippetPattern, CONSUME_CODE],
        [CONSUME_CODE, stopCodeSnippetPattern, DEFAULT, true],
        [CONSUME_CODE, anything, CONSUME_CODE],

        [DEFAULT, startParagraph, CONSUME_PARAGRAPH],
        [CONSUME_PARAGRAPH, startParagraph, CONSUME_PARAGRAPH],
        [CONSUME_PARAGRAPH, emptyLinePattern, DEFAULT],
    ];

    const transformationTable = {
        [DEFAULT]: block => block.join(' '),
        [CONSUME_TITLE]: block => block.join(' '),
        [CONSUME_BULLET]: block => block.join('\n'),
        [CONSUME_NUMBER]: block => block.join('\n'),
        [CONSUME_CODE]: block => block.join('\n'),
        [CONSUME_PARAGRAPH]: block => block.join(' ')
    };

    const acceptTable = {
        [DEFAULT]: false,
        [CONSUME_TITLE]: true,
        [CONSUME_BULLET]: true,
        [CONSUME_NUMBER]: true,
        [CONSUME_CODE]: true,
        [CONSUME_PARAGRAPH]: true
    };


    let block = [ ];
    let state = DEFAULT;
    let accept = false;
    let transform = transitionTable[state];

    const mustYield = (change, block) => change && block.length > 0;

    function computeState(currentState, line) {
        for (const [state, pattern, nextState, include] of transitionTable) {
            if (state === currentState && pattern(line))
                return [true, nextState, nextState !== currentState, include];
        }
        return [false, currentState, false, false];
    }

    for (const line of lines) {
        transform = transformationTable[state];
        const [success, nextState, change, include] = computeState(state, line);
        accept = acceptTable[nextState];

        if (mustYield(change, block)) {
            if (include) {
                block.push(line);
                yield transform(block);
                block = [ ];
            } else {
                yield transform(block);
                block = accept ? [line] : [ ];
            }
        } else if (success && accept) {
            block.push(line);
        }

        state = nextState;
    }

    if (accept && block.length > 0)
        yield transformationTable[state](block);
}

export function *streamOutTextChunks(
    text, makeRecognizer = makeDefaultRecognizer
) {
    const recognizer = makeRecognizer();
    for (const chunk of yieldBlock(text.split('\n')))
        yield *streamOutChunkElements(chunk, recognizer);
}

export function breakText(text) {
    return [...streamOutTextChunks(text)];
}
