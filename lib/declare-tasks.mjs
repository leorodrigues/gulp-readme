import makeGulpJson from '@leorodrigues/gulp-json';
import gChanged from 'gulp-changed';
import path from 'path';

const dirname = import.meta.dirname;

function makeDeployDefaultTemplatesTask(gulp, buildPath) {
    return () => gulp.src(path.join(dirname, '..', 'templates', '**', '*'))
        .pipe(gulp.dest(path.join(buildPath, 'templates')));
}

function makeDeployLocalTemplatesTask(gulp, buildPath) {
    const glob = path.join('.', '?(templates)', '**', '*');
    return () => gulp.src(glob)
        .pipe(gulp.dest(path.join(buildPath, 'templates')));
}

function makeReameMDTask(gulp, gJson, gReadme, buildPath, distPath) {
    const options = {
        viewsPath: path.join(buildPath, 'templates'),
        viewName: 'readme.njk',
        outputPath: 'README.md'
    };
    return () => gJson.yieldCollections()
        .pipe(gReadme.generateReadmeModel())
        .pipe(gReadme.renderTemplate(options))
        .pipe(gulp.dest(distPath));
}

function makeCollectReferencesTask(gulp, gJson, sources) {
    return () => gulp.src(sources)
        .pipe(gJson.parse())
        .pipe(gJson.collect('references'));
}

function makeCollectNotesTask(gulp, gJson, buildPath) {
    return () => gulp.src(path.join(buildPath, 'notes', '**', '*.json'))
        .pipe(gJson.parse())
        .pipe(gJson.collect('notes'));
}

function makeCollectDocsTask(gulp, gJson, buildPath) {
    return () => gulp.src(path.join(buildPath, 'sources', '**', '*.json'))
        .pipe(gJson.parse())
        .pipe(gJson.collect('api'));
}

function makeParseNotesTask(gulp, gJson, gReadme, buildPath, sources) {
    buildPath = path.join(buildPath, 'notes');
    return () => gulp.src(sources)
        .pipe(gChanged(buildPath, { extension: '.json' }))
        .pipe(gReadme.breakText())
        .pipe(gJson.toFile({ attributeName: 'brokenText' }))
        .pipe(gulp.dest(buildPath));
}

function makeExplainSourcesTask(gulp, gJson, gReadme, buildPath, sources) {
    buildPath = path.join(buildPath, 'sources');
    return () => gulp.src(sources, { allowEmpty: true })
        .pipe(gChanged(buildPath, {extension: '.json'}))
        .pipe(gReadme.explainSource())
        .pipe(gReadme.reduceExplanation())
        .pipe(gJson.toFile({ attributeName: 'explanation' }))
        .pipe(gulp.dest(buildPath));
}

export function declareTasks(
    prefix, gulp, gReadme, buildPath, {jsSources, notesSources, refSources}) {

    const gJson = makeGulpJson({ prettyPrint: true });

    const distPath = buildPath;

    buildPath = path.join(buildPath, 'readme');

    gulp.task(`${prefix}-deploy-default-templates`,
        makeDeployDefaultTemplatesTask(gulp, buildPath));

    gulp.task(`${prefix}-deploy-local-templates`,
        makeDeployLocalTemplatesTask(gulp, buildPath));

    gulp.task(`${prefix}-explain-sources`, makeExplainSourcesTask(
        gulp, gJson, gReadme, buildPath, jsSources));

    gulp.task(`${prefix}-parse-notes`, makeParseNotesTask(
        gulp, gJson, gReadme, buildPath, notesSources));

    gulp.task(`${prefix}-collect-jsdocs`, makeCollectDocsTask(
        gulp, gJson, buildPath));

    gulp.task(`${prefix}-collect-notes`, makeCollectNotesTask(
        gulp, gJson, buildPath));

    gulp.task(`${prefix}-collect-references`, makeCollectReferencesTask(
        gulp, gJson, refSources));

    gulp.task(`${prefix}-deploy-templates`, gulp.series([
        `${prefix}-deploy-default-templates`,
        `${prefix}-deploy-local-templates`
    ]));

    gulp.task(`${prefix}-prepare-all`, gulp.parallel([
        `${prefix}-deploy-templates`,
        `${prefix}-explain-sources`,
        `${prefix}-parse-notes`
    ]));

    gulp.task(`${prefix}-collect-json`, gulp.parallel([
        `${prefix}-collect-jsdocs`,
        `${prefix}-collect-notes`,
        `${prefix}-collect-references`
    ]));

    const task = makeReameMDTask(gulp, gJson, gReadme, buildPath, distPath);

    const deps = [`${prefix}-prepare-all`, `${prefix}-collect-json`];

    gulp.task(`${prefix}-readme-md`, gulp.series(deps, task));

    return [`${prefix}-readme-md`];
}
