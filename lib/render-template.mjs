const COMPONENT_NAME = 'render-template';

import Vinyl from 'vinyl';
import nunjucks from 'nunjucks';
import makeLogger from '@leorodrigues/logging';
import { Transform } from 'stream';

const log = makeLogger(COMPONENT_NAME);

function makeHandleRenderTemplate(environment, viewName, path) {
    return function handleRendering(model, encoding, next) {
        log.info('Yielding file from model.', 'outputPath', path);
        const output = new Vinyl({ path });
        output.contents = Buffer.from(environment.render(viewName, model));
        next(null, output);
    };
}

export function makeRenderTemplateApiCall() {
    return function renderTemplate(options = {}) {
        const {viewsPath, viewName, nunjucksOptions, outputPath} = options;
        const path = outputPath || 'out.html';
        log.info('Loading templates.', 'sourcePath', viewsPath);
        const environment = nunjucks.configure(viewsPath, nunjucksOptions);
        const handler = makeHandleRenderTemplate(environment, viewName, path);
        return new Transform({ objectMode: true, transform: handler });
    };
}
