const COMPONENT_NAME = 'reduce-explanation';

import { Transform } from 'stream';
import _ from 'lodash';
import { breakText } from './text-utils.mjs';
import makeLogger from '@leorodrigues/logging';

const log = makeLogger(COMPONENT_NAME);

const INSTANCE_TYPES = ['ArrayExpression', 'ObjectExpression', 'Literal'];

function removeUndefined(object) {
    for (const n in object)
        if (!object[n])
            delete object[n];
    return object;
}

function complementWithDescription(description, record) {
    if (description) record.description = breakText(description);
    return record;
}

function complementWithReturns(returns, record) {
    if (returns)
        record.returns = complementWithDescription(
            returns[0].description, { type: returns[0].type.names.join(',') });
    return record;
}

function complementWithExceptions(exceptions, record) {
    if (exceptions)
        record.exceptions = exceptions.map(t => complementWithDescription(
            t.description, { type: t.type.names.join(',') }));
    return record;
}

function simplifyBasicRecord(record) {
    const {name, description, scope, longname} = record;
    return complementWithDescription(
        description, {name, longName: longname, scope});
}

function transformParams(result, params) {
    if (!params) return;
    result.params = params.map(p => complementWithDescription(p.description, {
        name: p.name,
        type: p.type.names.join(',')
    }));
}

function simplifyFunctionRecord(record) {
    const result = simplifyBasicRecord(record);
    transformParams(result, record.params);
    complementWithReturns(record.returns, result);
    return complementWithExceptions(record.exceptions, result);
}

function simplifyClassRecord(classRecord) {
    const {
        name,
        description,
        classdesc,
        scope,
        longname,
        params,
        augments
    } = classRecord;
    const result = complementWithDescription(classdesc, {
        name,
        longName: longname,
        scope,
        ancestors: augments
    });
    if (params) {
        result.constructorMethod = complementWithDescription(description, {
            scope,
            kind: 'function',
            name: 'constructor',
            longName: `${longname}#constructor`
        });
        transformParams(result.constructorMethod, params);
    }
    return result;
}

function extractModule(explanation) {
    const record = explanation.find(e => e.kind === 'module');
    if (!record)
        return {};
    const {name, description, longname} = record;
    const result = {
        name,
        longName: longname,
        description: breakText(description)
    };
    if (record.author)
        result.authors = record.author;
    return result;
}

function onlyOfMetaCodeTypes(...types) {
    return e => (e.meta && types.some(t => e.meta.code.type === t));
}

function onlyFromClass(classLongName) {
    return e => e.memberof === classLongName;
}

function collect(explanation, criterion = () => true, transform = e => e) {
    return explanation.filter(criterion)
        .map(transform)
        .map(removeUndefined);
}

function collectMethods(explanation, classLongName) {
    return explanation
        .filter(onlyOfMetaCodeTypes('MethodDefinition'))
        .filter(onlyFromClass(classLongName))
        .map(simplifyFunctionRecord)
        .map(removeUndefined);
}

function collectFields(explanation, classLongName) {
    return explanation
        .filter(onlyOfMetaCodeTypes('ClassProperty'))
        .filter(onlyFromClass(classLongName))
        .map(simplifyFunctionRecord)
        .map(removeUndefined);
}

function collectClasses(explanation) {
    const result = collect(explanation,
        onlyOfMetaCodeTypes('ClassDeclaration'), simplifyClassRecord);
    result.forEach(e => {
        e.methods = collectMethods(explanation, e.longName);
        e.fields = collectFields(explanation, e.longName);
    });
    return result;
}

function transform(explanation) {
    explanation = explanation.filter(e => !e.undocumented);

    const moduleRecord = extractModule(explanation);

    if (_.isEqual(moduleRecord, {}))
        return;

    const constants = collect(explanation,
        onlyOfMetaCodeTypes(...INSTANCE_TYPES), simplifyBasicRecord);

    const functions = collect(explanation,
        onlyOfMetaCodeTypes('FunctionDeclaration'), simplifyFunctionRecord);

    const classes = collectClasses(explanation);

    return {module: moduleRecord, constants, functions, classes};
}

function makeHandleReduceExplanation() {
    return function handleReduction(file, encoding, next) {
        const explanation = transform(file.explanation);
        if (!explanation) {
            log.warn('Rejected explanation of file.', 'path', file.path);
            return next();
        }
        log.info('Accepted explanantion of file.', 'path', file.path);
        file.explanation = explanation;
        next(null, file);
    };
}

export function makeReduceExplanationApiCall() {
    return function reduceExplanation() {
        return new Transform({
            objectMode: true, transform: makeHandleReduceExplanation()
        });
    };
}
