const COMPONENT_NAME = 'generate-readme-model';

import makeLogger from '@leorodrigues/logging';
import { Transform } from 'stream';

const log = makeLogger(COMPONENT_NAME);

function computeSummary({ constants, functions, classes }) {
    const menu = [];
    const wrapBasic = o => ({
        text: o.name,
        ref: o.longName,
        type: 'link'
    });

    menu.push(...constants.map(wrapBasic));

    menu.push(...functions.map(wrapBasic));

    menu.push(...classes.map(c => {
        const item = wrapBasic(c);
        const submenu = item.menu = [];
        if (c.fields)
            submenu.push(...c.fields.map(wrapBasic));
        if (c.constructorMethod)
            submenu.push(wrapBasic(c.constructorMethod));
        if (c.methods)
            submenu.push(...c.methods.map(wrapBasic));
        return item;
    }));

    return menu;
}

function flattenReferences(referenceList) {
    const result = {};
    referenceList.forEach(r =>
        Object.keys(r).forEach(k =>
            result[k] = r[k]));
    return result;
}

function tryUpdatingLink(model, references) {
    if (model.type && model.type === 'link' || model.ref)
        model.ref = references[model.ref] || `#${model.ref}`;
}

function resolveLinks(model, references) {
    if (model instanceof Array) {
        for (const m of model)
            resolveLinks(m, references);
        return;
    }
    if (model instanceof Object) {
        tryUpdatingLink(model, references);
        for (const k in model)
            if (model.hasOwnProperty(k)) // eslint-disable-line
                resolveLinks(model[k], references);
        return;
    }
}

function *generateForewordContent(textSequence) {
    for (const part of textSequence)
        for (const element of part)
            yield element;
}

function *generateElements(type, collection, transform = (t, e) => e) {
    if (collection && collection.length)
        for (const element of collection) {
            if (!element) continue;
            yield transform(type, element);
        }
}

function *makeMainSummary(modules) {
    for (const m of modules)
        yield { type: 'link', text: m.module.name, ref: m.module.longName };
}

function complementWith(name, part, record) {
    if (part && (!(part instanceof Array) || part.length))
        record[name] = part;
}

function makeFunction(type, record) {
    const {
        description, name, longName, params, returns, scope, exceptions
    } = record;
    const result = { type, name, target: longName, description, scope };
    complementWith('params', params, result);
    complementWith('returns', returns, result);
    complementWith('exceptions', exceptions, result);
    return result;
}

function makeInstance(type, record) {
    const { description, name, longName, scope } = record;
    return {
        type,
        name,
        target: longName,
        description,
        scope
    };
}

function makeConstructor(type, record) {
    const { description, name, longName, scope, params } = record;
    return {
        type,
        name,
        target: longName,
        scope,
        params,
        description
    };
}

function makeMethod(type, record) {
    const {
        description, name, longName, scope, params, returns, exceptions
    } = record;
    const result = {type, name, target: longName, scope, description};
    complementWith('params', params, result);
    complementWith('returns', returns, result);
    complementWith('exceptions', exceptions, result);
    return result;
}

function makeClass(type, record) {
    const {
        description,
        name,
        longName,
        scope,
        ancestors,
        constructorMethod,
        methods
    } = record;
    const constructors = [constructorMethod];
    const content = [
        ...generateElements('constructor', constructors, makeConstructor),
        ...generateElements('method', methods, makeMethod)
    ];
    return {
        type,
        name,
        target: longName,
        description,
        scope,
        ancestors,
        content
    };
}

function *generateApiReferenceContent(moduleSequence) {
    const tuples = moduleSequence.map(m => ({ s: computeSummary(m), m }));
    const mainSummary = [...makeMainSummary(tuples.map(({m}) => m))];
    if (mainSummary.length)
        yield {type: 'summary', content: mainSummary};
    for (const {s, m} of tuples) {
        const summary = s;
        if (m.module) {
            const {description, name, longName} = m.module;
            const target = longName;
            const content = [
                ...generateElements('instance', m.constants, makeInstance),
                ...generateElements('function', m.functions, makeFunction),
                ...generateElements('class', m.classes, makeClass)
            ];
            yield {type: 'module', summary, name, target, description, content};
        }
    }
}

function *generateSection(title, target, sectionContent, generateContent) {
    if (sectionContent)
        yield {
            title,
            target,
            content: [...generateContent(sectionContent)]
        };
}

export function transformIntoReadme(collections) {
    const { notes, api } = collections;
    const readme = {
        sections: [
            ...generateSection(
                'Foreword', 'foreword', notes, generateForewordContent),
            ...generateSection(
                'Api Reference', 'reference', api, generateApiReferenceContent)
        ]
    };

    let {references} = collections;
    if (references) {
        references = flattenReferences(references);
        resolveLinks(readme, references);
        readme.references = references;
    }

    return readme;
}

function makeHandleGenerateReadmeModel() {
    return function handleGeneration(sections, encoding, next) {
        log.info('Generating readme from sections map.');
        const readmeModel = transformIntoReadme(sections);
        next(null, readmeModel);
    };
}

export function makeGenerateReadmeModelApiCall() {
    return function generateReadmeModel() {
        return new Transform({
            objectMode: true, transform: makeHandleGenerateReadmeModel()
        });
    };
}
