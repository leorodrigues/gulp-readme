const COMPONENT_NAME = 'explain-source';
import PluginError from 'plugin-error';
import jsdoc from 'jsdoc-api';
import makeLogger from '@leorodrigues/logging';
import { Transform } from 'stream';

const log = makeLogger(COMPONENT_NAME);

function makeHandleExplainSource() {
    return function handleExplanation(file, encoding, callback) {
        if (file.isNull())
            return callback(null, file);
        if (file.isStream())
            return callback(new PluginError(
                COMPONENT_NAME, 'Streaming not supported'));
        log.info('Explaining file.', 'path', file.path);
        file.explanation = jsdoc.explainSync({ source: file.contents });
        callback(null, file);
    };
}

export function makeExplainSourceApiCall() {
    return function explainSource() {
        return new Transform({
            objectMode: true, transform: makeHandleExplainSource()
        });
    };
}
