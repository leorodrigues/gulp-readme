const COMPONENT_NAME = 'break-text';

import makeLogger from '@leorodrigues/logging';
import PluginError from 'plugin-error';
import { breakText } from './text-utils.mjs';
import { Transform } from 'stream';

const log = makeLogger(COMPONENT_NAME);

function makeHandleBreakText() {
    return function handleBreakText(file, encoding, callback) {
        if (file.isNull())
            return callback(null, file);
        if (file.isStream())
            return callback(new PluginError(
                COMPONENT_NAME, 'Streaming not supported'));
        log.info(`Breaking file '${file.relative}'.`);
        file.brokenText = breakText(file.contents.toString());
        callback(null, file);
    };
}

export function makeBreakTextApiCall() {
    return function breakText() {
        return new Transform({
            objectMode: true,
            transform: makeHandleBreakText()
        });
    };
}
