import { makeBreakTextApiCall } from './lib/break-text.mjs';
import { makeExplainSourceApiCall } from './lib/explain-source.mjs';
import { makeReduceExplanationApiCall } from './lib/reduce-explanation.mjs';
import { makeRenderTemplateApiCall } from './lib/render-template.mjs';

import { makeGenerateReadmeModelApiCall }
    from './lib/generate-readme-model.mjs';

export { declareTasks } from './lib/declare-tasks.mjs';

export function makeGulpReadme() {
    const breakText = makeBreakTextApiCall();
    const explainSource = makeExplainSourceApiCall();
    const reduceExplanation = makeReduceExplanationApiCall();
    const generateReadmeModel = makeGenerateReadmeModelApiCall();
    const renderTemplate = makeRenderTemplateApiCall();
    return {
        breakText,
        explainSource,
        generateReadmeModel,
        reduceExplanation,
        renderTemplate
    };
}
