import js from '@eslint/js';
import globals from 'globals';
import mochaPlugin from 'eslint-plugin-mocha';

export default [
    js.configs.recommended,
    {
        'rules': {
            'no-unused-vars': ['warn'],
            'indent': ['error', 4, {
                'SwitchCase': 1
            }],
            'quotes': ['error', 'single'],
            'semi': ['error', 'always'],
            'max-len': ['error', 80],
            'complexity': ['error', 9],
            'camelcase': ['error', {
                'properties': 'always'
            }],
            'new-cap': ['error', {
                'capIsNew': true,
                'newIsCap': true,
                'capIsNewExceptions': [
                    'Resource', 'ObjectId'
                ]
            }],
            'newline-per-chained-call': ['error', {
                'ignoreChainWithDepth': 2
            }],
            'prefer-const': ['error', {
                'destructuring': 'any',
                'ignoreReadBeforeAssign': false
            }],
            'for-direction': ['off']
        },
        'languageOptions': {
            'ecmaVersion': 2022,
            'sourceType': 'module',
            'globals': {
                ...globals.node
            }
        },
        'ignores': [
            'coverage/**/*',
            'node_modules/**/*'
        ]
    },
    {
        ...mochaPlugin.configs.flat.recommended,
        'files': ['test/**/*.mjs'],
        'rules': {
            'mocha/no-pending-tests': 'off',
            'max-len': ['error', 100]
        }
    }
];
